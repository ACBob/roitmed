extends Node

onready var _states = {}
onready var _current_state: String

func add_state(state) -> void:
	_states[state] = _states.size()
	return


func enter_state(new_state: String, old_state: String) -> void:
	pass

func exit_state(old_state: String, new_state: String) -> void:
	pass

func in_state(cur_state) -> void:
	pass

func get_state_change():
	pass

func _change_state(new_state: String) -> void:
	var old_state: String = _current_state
	
	enter_state(new_state, old_state)
	_current_state = new_state
	exit_state(old_state, new_state)

func _physics_process(delta):
	var new_state = get_state_change()
	if new_state and new_state != _current_state:
		_change_state(new_state)
	else:
		in_state(_current_state)
