extends KinematicBody2D

var health: int
onready var visual = $Visual

func damage(dmg: int):
	visual.animation = "hit"
	visual.play()
	
	health -= dmg

func die():
	print("kill")

func _on_Visual_animation_finished():
	match visual.animation:
		"hit":
			visual.animation = "idle"
	
	if health <= 0:
		die()
