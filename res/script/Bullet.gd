extends Area2D

const SPEED = 6 * globals.TILE_SIZE

var friendly = [] # Don't hurt
var ignore = [] # Don't collide
var velocity = Vector2.ZERO

func go(angle: float):
	velocity = Vector2(1,0).rotated(angle) * SPEED

func goto(position: Vector2):
	look_at(position)
	velocity = Vector2(1,0).rotated(rotation) * SPEED
	rotation = 0

func _physics_process(delta):
	position += velocity*delta

func collide(body):
	if body in ignore:
		return # We don't collide with it
	
	if not body in friendly:
		if body.has_method("damage"):
			body.damage(10)
	
	# Die
	queue_free()
