extends "res://res/script/BaseHurtable.gd"

var velocity = Vector2.ZERO

var target: Node2D = null

func _ready():
	health = 15

func die():
	queue_free()

func _physics_process(delta):
	
	if not target:
		pass
	
	move_and_slide(velocity)
