extends Area2D

# Find some better way of instancing all the entities
var DUMMY = load("res://res/scene/dummy.tscn")
var ROITER = load("res://res/scene/enemies/Roitmed.tscn")

export(String) var RoomName = "Nowhere"
var IsActive = false

onready var entity_positions = $Entities
onready var entities = Node.new()

func _ready():
	add_child(entities)
	entity_positions.visible = false

func get_room_name() -> String:
	return RoomName

func get_active() -> bool:
	return IsActive


func enter():
	for entity_position in entity_positions.get_used_cells():
		var new_ent
		if entity_positions.get_cellv(entity_position) == 0:
			# Dummy spawn
			new_ent = DUMMY.instance()
		elif entity_positions.get_cellv(entity_position) == 1:
			## Roiter Spawn
			new_ent = ROITER.instance()
		
		
		if new_ent:
			entities.add_child(new_ent)
			new_ent.position = entity_position * globals.TILE_SIZE
			#new_ent.position += Vector2(new_ent.get_node("Collision").shape.extents.x,0)

func exit():
	for entity in entities.get_children():
		entity.queue_free()
