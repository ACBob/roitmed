extends BaseStateMachine

onready var player = self.get_parent()

onready var land_timer = Timer.new()

func _ready():
	add_state("idle")
	add_state("walk")
	add_state("jump")
	add_state("fall")
#	add_state("slip")
	add_state("land")
	
	_current_state = "idle"
	
	add_child(land_timer)
	land_timer.wait_time = 0.5
	land_timer.one_shot = true


func in_state(state):
	if !(_current_state in ["land"]):
		player._handle_input()
	player.set_anim(state)
	

func get_state_change():
	match _current_state:
		"idle":
			if player.velocity.x != 0:
#				if player.move_direction:
				return "walk"
#				return "slip"
			if player.should_jump():
				return "jump"
			if player.velocity.y > 0:
				return "fall"
		"walk":
			if player.velocity.x == 0:
				return "idle"
			if player.should_jump():
				return "jump"
#			if !player.move_direction:
#				return "slip"
			if player.velocity.y > 0:
				return "fall"
		"jump":
			if player.velocity.y > 0:
				return "fall"
		"fall":
			if player.on_floor():
				if player.ovelocity.y >= 450:
					land_timer.start()
					return "land"
				return "idle"
#		"slip":
##			if player.move_direction:
##				return "walk"
#			if player.velocity.x == 0:
#				return "idle"
		"land":
			if land_timer.time_left == 0:
				return "idle"

func enter_state(new_state, old_state):
	match new_state:
		"jump":
			player.jump()
