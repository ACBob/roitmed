extends KinematicBody2D

# TODO: Sort-out ramps!


const UP = Vector2.UP
const MOVE_SPEED = globals.TILE_SIZE * 6 # 6 tiles per second

const JUMP_HEIGHT_MAX = globals.TILE_SIZE * 6 # 6 Tiles up
const JUMP_HEIGHT_MIN = globals.TILE_SIZE * 0.8 # 0.8 tiles up
const JUMP_LENGTH = .75 # How long the jump should take place

var Bullet = load("res://res/scene/Bullet.tscn")

var jump_velocity_max
var jump_velocity_min
var gravity

onready var state_machine = $StateMachine
onready var visual = $Visual
onready var current_room: Area2D = null

onready var rooms = owner.get_node("RoomMap").get_children()

onready var player_camera = get_node("Camera")
onready var arm = get_node("Arm")

var health = 99
var canisters = 8

var velocity: Vector2 = Vector2.ZERO
var ovelocity: Vector2 = Vector2.ZERO # Stores the previous frame's velocity
var facing: bool = false # true - left, false - right
var move_direction: int = 0 # what direction are we moving in?

func _ready():
	gravity = 2 * JUMP_HEIGHT_MAX / pow(JUMP_LENGTH, 2)
	jump_velocity_max = -sqrt(2 * gravity * JUMP_HEIGHT_MAX)
	jump_velocity_min = -sqrt(2 * gravity * JUMP_HEIGHT_MIN)

func _physics_process(delta):
	velocity.y += gravity * delta
	
	ovelocity = velocity
	velocity = move_and_slide(velocity, UP, 90)

func _handle_input():
	
	move_direction = 0
	
	if Input.is_action_pressed("mv_lf"):
		facing = true
		move_direction = -1
	if Input.is_action_pressed("mv_rt"):
		facing = false
		move_direction = 1
	
	arm.rotation = 0.0
	arm.look_at(get_global_mouse_position())
	if arm.rotation_degrees > 90 or arm.rotation_degrees < -90:
		arm.get_node("Visual").flip_v = true
	else:
		arm.get_node("Visual").flip_v = false
	
	if Input.is_action_just_pressed("mv_sht"):
		var bullet = Bullet.instance()
		owner.add_child(bullet)
		bullet.position = arm.get_node("PewPoint").global_position
		# Add to both just incase
		bullet.friendly = [self]
		bullet.ignore = [arm,self]
		bullet.go(arm.rotation)
	
	if Input.is_action_just_released("mv_jmp"):
		if velocity.y < jump_velocity_min:
			velocity.y = jump_velocity_min
	
	visual.flip_h = facing
	
	velocity.x = MOVE_SPEED * move_direction

func set_anim(anim):
	visual.animation = anim

func update_current_room(new_room):
	if not "RoomName" in new_room:
		return # Duck-typing
	var anim = false # Play animation when switching?
	
	if current_room:
		anim = true
		current_room.IsActive = false
	
	if new_room != current_room:
		# Updates Position w/ animation
		
		new_room.enter()
		if current_room:
			current_room.exit()
		
		current_room = new_room
		current_room.IsActive = true
		
		var cam_limits = [
			current_room.position.y,
			current_room.position.y + (current_room.get_node("Shape").shape.extents.y*2),
			current_room.position.x,
			current_room.position.x + (current_room.get_node("Shape").shape.extents.x*2)
		]
		if anim:
			player_camera.set_limits(cam_limits)
		else:
			player_camera.limits = cam_limits
			player_camera.limit_top = cam_limits[0]
			player_camera.limit_bottom = cam_limits[1]
			player_camera.limit_left = cam_limits[2]
			player_camera.limit_right = cam_limits[3]

func get_current_room():
	return current_room

func on_floor() -> bool:
	return is_on_floor()

func should_jump() -> bool:
	if Input.is_action_pressed("mv_jmp") && on_floor():
		return true
	return false

func jump():
	velocity.y = jump_velocity_max

func damage(dmg: int):
	health -= dmg
	if health <= 0:
		var dmglf = abs(health)
		canisters -= 1
		health = 99
		if dmglf > 0:
			damage(dmglf)
		if canisters < 0:
			print("game over, bro")
