extends Control

const DEBUG = true

export(NodePath) var PlayerPath
onready var Player = get_node(PlayerPath)
onready var PlayerStateMachine = Player.get_node("StateMachine")

onready var Debug = get_node("DebugPanel")
onready var DebugLabel = Debug.get_node("StateInfo")
onready var DebugVelocity = Debug.get_node("VelocityInfo")

onready var Root = get_node("BasePanel")
onready var RoomName = Root.get_node("RoomName")
onready var PlayerHealth = Root.get_node("Energy")
onready var PlayerTanks = Root.get_node("EnergyTanks")

func _ready():
	pass

func _physics_process(delta):
	Debug.visible = DEBUG
	
	DebugLabel.text = PlayerStateMachine._current_state
	DebugVelocity.text = str(Player.velocity)
	
	PlayerHealth.text = str(Player.health)
	PlayerTanks.visible_characters = Player.canisters
	
	if Player.current_room:
		RoomName.text = Player.get_current_room().get_room_name()
