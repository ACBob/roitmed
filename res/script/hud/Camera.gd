extends Camera2D

var old_limits = []
var limits = []

onready var tween = $Tween
onready var player = get_parent()

func _ready():
	old_limits = [
		limit_top,
		limit_bottom,
		limit_left,
		limit_right
	]
	limits = old_limits

func set_limits(new_limits):
	get_tree().paused = true
	
	limit_top = -10000000
	limit_bottom = 10000000
	limit_left = -10000000
	limit_right = 10000000
	
	old_limits = limits
	limits = new_limits
	
	var new_trans
	var old_trans
	if !player.facing:
		old_trans = Transform2D(
			Vector2(1,0),
			Vector2(0,1),
			Vector2(old_limits[3]-(get_viewport().size.x/2),old_limits[0]+(get_viewport().size.y/2))
		)
		new_trans = Transform2D(
			Vector2(1,0),
			Vector2(0,1),
			Vector2(new_limits[2]+(get_viewport().size.x/2),new_limits[0]+(get_viewport().size.y/2))
		)
	else:
		old_trans = Transform2D(
			Vector2(1,0),
			Vector2(0,1),
			Vector2(old_limits[2]+(get_viewport().size.x/2),old_limits[0]+(get_viewport().size.y/2))
		)
		new_trans = Transform2D(
			Vector2(1,0),
			Vector2(0,1),
			Vector2(new_limits[3]-(get_viewport().size.x/2),new_limits[0]+(get_viewport().size.y/2))
		)
	global_transform = old_trans
	tween.interpolate_property(self, "global_transform", old_trans, new_trans, 1.0, tween.TRANS_LINEAR)
	tween.start()

func _finish_set_limits():
	get_tree().paused = false
	
	# Force our position back onto 0,0 for the player
	transform = Transform2D()
	# Set our limits, we don't earlier because it lets us s l i d e
	limit_top = limits[0]
	limit_bottom = limits[1]
	limit_left = limits[2]
	limit_right = limits[3]
	
	print("Went to ", limit_top, " ", limit_bottom, " ", limit_left, " ", limit_right)
	print("Transform ", global_transform)


func _finish_tween(object, key):
	_finish_set_limits()
	pass
